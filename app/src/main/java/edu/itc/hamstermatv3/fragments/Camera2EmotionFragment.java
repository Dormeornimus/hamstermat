package edu.itc.hamstermatv3.fragments;


import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.fragment.app.Fragment;
import edu.itc.hamstermatv3.R;
import edu.itc.hamstermatv3.ai.tensorflowold.Classifier;
import edu.itc.hamstermatv3.ai.tensorflowold.TensorFlowClassifier;

import static org.opencv.android.CameraBridgeViewBase.CAMERA_ID_FRONT;

public class CameraEmotionFragment extends Fragment implements CameraBridgeViewBase.CvCameraViewListener2 {

    private static final int PIXEL_WIDTH = 150;

    // Used for logging success or failure messages
    private static final String TAG = CameraEmotionFragment.class.getSimpleName();

    // Loads camera view of OpenCV for us to use. This lets us see using OpenCV
    private CameraBridgeViewBase mOpenCvCameraView;

    // Used in Camera selection from menu (when implemented)
    private boolean mIsJavaCamera = true;
    private MenuItem mItemSwitchCamera = null;

    // These variables are used (at the moment) to fix camera orientation from 270degree to 0degree
    private Mat mRgba;
    private Mat mRgbaF;
    private Mat mRgbaT;

    private final Size sizeImage = new Size(150,150);

    private CascadeClassifier faceCascade;

    private Integer absoluteFaceSize;

    private List<Classifier> mClassifiers = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);

        loadModel();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.show_camera, container, false);
        mOpenCvCameraView =  view.findViewById(R.id.show_camera_activity_java_surface_view);
        mOpenCvCameraView.setCameraIndex(CAMERA_ID_FRONT);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);

        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(getContext()) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    try {
                        // load cascade file from application resources
                        InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
                        File cascadeDir = getActivity().getDir("cascade", Context.MODE_PRIVATE);
                        File mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
                        FileOutputStream os = new FileOutputStream(mCascadeFile);

                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        is.close();
                        os.close();

                        faceCascade = new CascadeClassifier(mCascadeFile.getAbsolutePath());
                        if (faceCascade.empty()) {
                            Log.e(TAG, "Failed to load cascade classifier");
                            faceCascade = null;
                        } else
                            Log.i(TAG, "Loaded cascade classifier from " + mCascadeFile.getAbsolutePath());


                        cascadeDir.delete();

                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
                    }
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public CameraEmotionFragment() {
        faceCascade = new CascadeClassifier();
        absoluteFaceSize = 0;
        Log.i(TAG, "Instantiated new " + this.getClass());

    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, getContext(), mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {

        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mRgbaF = new Mat(height, width, CvType.CV_8UC4);
        mRgbaT = new Mat(width, width, CvType.CV_8UC4);
    }

    public void onCameraViewStopped() {
        mRgba.release();
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        // TODO Auto-generated method stub
        mRgba = inputFrame.rgba();

        // Rotate mRgba 90 degrees
        Core.transpose(mRgba, mRgbaT);
        Imgproc.resize(mRgbaT, mRgbaF, mRgbaF.size(), 0,0, 0);
        //|Core.flip(mRgbaF, mRgba, 0 );

        Mat grey = new Mat(mRgba.height(), mRgba.width(), CvType.CV_8UC3);
        Imgproc.cvtColor(mRgba,grey,Imgproc.COLOR_RGB2GRAY);

        return toDisplay(mRgba); // This function must return
    }

    private Mat toDisplay(Mat inc){
        MatOfRect faces = new MatOfRect();
        Mat grey = new Mat(inc.height(), inc.width(), CvType.CV_8UC3);
        Imgproc.cvtColor(inc,grey,Imgproc.COLOR_RGB2GRAY);

        Imgproc.equalizeHist(grey,grey);

        if(this.absoluteFaceSize == 0) {
            Integer heigh = grey.rows();
            if (Math.round(heigh * 3.0f) > 0)
                this.absoluteFaceSize = Math.round(heigh * 0.3f);
        }

        this.faceCascade.detectMultiScale(grey, faces, 1.5, 1, 0
                        | Objdetect.CASCADE_SCALE_IMAGE,
                new Size( this.absoluteFaceSize, this.absoluteFaceSize), new Size());

        Rect facesArray[] = faces.toArray();

        Mat toReturn = inc;

        for (int i = 0; i < facesArray.length ; i++){

            Imgproc.rectangle(toReturn, facesArray[i].tl(), facesArray[i].br(), new Scalar(0, 255, 0), 2);

            double pos_x = (facesArray[i].tl().x - 10 > 0) ? facesArray[i].tl().x - 10 : 0 ;
            double pos_y = (facesArray[i].tl().y - 10 > 0) ? facesArray[i].tl().x - 10 : 0 ;

            Mat cropped = new Mat(toReturn, facesArray[i]);
            Imgproc.resize(cropped, cropped, sizeImage, 0, 0, Imgproc.INTER_LINEAR);

            String lordvaldomero;

            if (facesArray.length > 0)
                lordvaldomero = mClassifiers.get(0).recognizetoString(pixelator3(cropped));
            else
                lordvaldomero = "?";

            Imgproc.putText(toReturn, lordvaldomero, new Point(pos_x,pos_y), Core.FONT_HERSHEY_PLAIN, 1.0, new Scalar(0, 255, 0), 2 );


        }
        return inc;
    }

    private float[] pixelator3 (Mat inc) {

        Bitmap bmp = Bitmap.createBitmap(inc.cols(), inc.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(inc, bmp);

        if (bmp == null) {
            return null;
        }


        //System.out.println(bmp.toString());

        int width = bmp.getWidth();
        int height = bmp.getHeight();

        int[] pixels = new int[width * height];
        bmp.getPixels(pixels, 0, width, 0, 0, width, height);

        float[] retPixels = new float[pixels.length];
        for (int i = 0; i < pixels.length; ++i) {
            // Set 0 for white and 255 for black pixel
            int pix = pixels[i];
            int b = pix & 0xff;
            retPixels[i] = (float)((0xff - b)/255.0);
        }


        //List<Float> somelist = new ArrayList<>();
        //for (int j = 0; j < retPixels.length; j++) {
        //    somelist.add(retPixels[j]);
        //}

        Log.i("Current Image Data", inc.toString());
        return retPixels;
    }



    private float[] pixelator2(Mat inc) {
        Mat aux = new Mat();
        aux = inc;
        aux.convertTo(aux, CvType.CV_32F);
        float buff[] = new float[(int)aux.total() * aux.channels()];
        inc.get(0, 0, buff);
        return buff;
    }

    private float[] pixelator(Mat inc) {
        int width = inc.width();
        int height = inc.height();

        List<Float> retPixels = new ArrayList<>();
        for (int i = 0 ; i < inc.rows(); i++){
            for (int j = 0 ; j < inc.cols(); i++){
                retPixels.add((float)inc.get(i,j)[0]);
            }
        }
        float [] returning = new float[retPixels.size()];
        for (int z = 0 ; z < inc.rows(); z++){
            returning[z] = retPixels.get(z);
        }

        return returning;
    }

    private void loadModel() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mClassifiers.add(
                            TensorFlowClassifier.create(getActivity().getAssets(), "TensorFlow",
                                    "my_model.h5.pb", "labels2.txt", PIXEL_WIDTH,
                                    "conv1d_1_input", "output_node0", true));

                } catch (final Exception e) {
                    //if they aren't found, throw an error!
                    throw new RuntimeException("Error initializing classifiers!", e);
                }
            }
        }).start();
    }


}
