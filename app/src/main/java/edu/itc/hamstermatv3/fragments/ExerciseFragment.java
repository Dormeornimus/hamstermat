package edu.itc.hamstermatv3.fragments;

import android.app.Fragment;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nex3z.fingerpaintview.FingerPaintView;

import java.io.IOException;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import edu.itc.hamstermatv3.R;
import edu.itc.hamstermatv3.ai.inference.Result;
import edu.itc.hamstermatv3.ai.recognizers.NumbersClassifier;
import edu.itc.hamstermatv3.db.ExerciseRepository;
import edu.itc.hamstermatv3.ui.adapters.LineAdapter;
import edu.itc.hamstermatv3.utilities.CountDownTimerWithPause;
import edu.itc.hamstermatv3.utilities.ImageUtil;

// TODO Finish implementation with inference system.
public class ExerciseFragment extends Fragment  implements LineAdapter.LineAdapterOnClickHandler, View.OnDragListener {
    private static final String LOG_TAG = ExerciseFragment.class.getSimpleName();
    private RecyclerView mFirstLineRecyclerView;
    private RecyclerView mSecondLineRecyclerView;
    private RecyclerView mFirstAnswerRecyclerView;
    private RecyclerView mSecondAnswerRecyclerView;
    private RecyclerView mThirdAnswerRecyclerView;
    private RecyclerView mDigitNumberRecyclerView;
    private TextView mTimerView;
    private TextView mHelpsView;
    private TextView mErrorsView;
    private ImageView mEnterButton;
    private ImageView mClearButton;
    private ImageView mTimerButton;
    private ExerciseCountdown mExerciseCountdown;
    private ExerciseRepository mExerciseRepository;
    private FingerPaintView mFpvPaint;
    private NumbersClassifier mNumbersClassifier;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mExerciseRepository = ExerciseRepository.getInstance();
        try {
            mNumbersClassifier = new NumbersClassifier(this.getActivity());
        } catch (IOException e) {
            Toast.makeText(this.getActivity(), R.string.failed_to_create_classifier, Toast.LENGTH_LONG).show();
            Log.e(LOG_TAG, "init(): Failed to create tflite model", e);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        createCountdown();
        return inflater.inflate(R.layout.fragment_exercise, container, false);

    }

    public void createCountdown(){
        mExerciseCountdown = new ExerciseCountdown(mExerciseRepository.getmTime()*1000,1000,true);
    }

    @Override
    public void onPause() {
        mExerciseCountdown.pause();
        super.onPause();
    }

    @Override
    public void onViewCreated(View view,Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirstLineRecyclerView =  view.findViewById(R.id.firstLine);
        mSecondLineRecyclerView = view.findViewById(R.id.secondLine);
        mFirstAnswerRecyclerView = view.findViewById(R.id.firstAnswer);
        mSecondAnswerRecyclerView = view.findViewById(R.id.secondAnswer);
        mThirdAnswerRecyclerView = view.findViewById(R.id.thirdAnswer);
        mDigitNumberRecyclerView = view.findViewById(R.id.digitNumbers);
        mHelpsView = view.findViewById(R.id.helps);
        mErrorsView = view.findViewById(R.id.errors);
        mTimerButton = view.findViewById(R.id.btn_timer);
        mTimerButton.setOnClickListener(onTimer);
        mEnterButton = view.findViewById(R.id.btn_detect);
        mEnterButton.setOnClickListener(onDetectPressed);
        mClearButton = view.findViewById(R.id.btn_clear);
        mClearButton.setOnClickListener(onCleanPressed);
        mFpvPaint = view.findViewById(R.id.fpv_paint);
        mTimerView = view.findViewById(R.id.timer);
        updateView();
        mFirstAnswerRecyclerView.setTag("FirstAnswer");
        mSecondAnswerRecyclerView.setTag("SecondAnswer");
        mThirdAnswerRecyclerView.setTag("ThirdAnswer");
        mFirstAnswerRecyclerView.setOnDragListener(this);
        mSecondLineRecyclerView.setOnDragListener(this);
        mThirdAnswerRecyclerView.setOnDragListener(this);
        if(mExerciseCountdown.isPaused()){
            mExerciseCountdown.resume();
        }
    }

    public void setAdapterView(RecyclerView lineRecyclerView, List<Integer> data, boolean dragable) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        layoutManager.setStackFromEnd(true);
        lineRecyclerView.setLayoutManager(layoutManager);
        lineRecyclerView.setHasFixedSize(true);
        LineAdapter lineAdapter = new LineAdapter(this,dragable);
        lineAdapter.setNumberData(data);
        lineRecyclerView.setAdapter(lineAdapter);
        LayoutAnimationController rightToLeftAnimation = AnimationUtils.loadLayoutAnimation(lineRecyclerView.getContext(), R.anim.layout_right_left);
        lineRecyclerView.setLayoutAnimation(rightToLeftAnimation);
        lineRecyclerView.getAdapter().notifyDataSetChanged();
        lineRecyclerView.scheduleLayoutAnimation();
    }

    public void updateView(){
        setAdapterView(mFirstLineRecyclerView, mExerciseRepository.getmFirstOperand(),false);
        setAdapterView(mSecondLineRecyclerView, mExerciseRepository.getmSecondOperand(),false);
        setAdapterView(mFirstAnswerRecyclerView, mExerciseRepository.getmFirstAnswer(),true);
        setAdapterView(mSecondAnswerRecyclerView, mExerciseRepository.getmSecondAnswer(),true);
        setAdapterView(mThirdAnswerRecyclerView, mExerciseRepository.getmThirdAnswer(),true);
        mErrorsView.setText(mExerciseRepository.getmErrors().toString());
        mHelpsView.setText(mExerciseRepository.getmHelps().toString());
    }
    @Override
    public boolean onDrag(View view, DragEvent dragEvent) {

        final int action = dragEvent.getAction();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                if (dragEvent.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                    return true;
                }
                return false;

            case DragEvent.ACTION_DRAG_ENTERED:
                Log.d("LineEvent: ", dragEvent.getClipDescription().getLabel().toString());
                return true;

            case DragEvent.ACTION_DRAG_LOCATION:
                return true;

            case DragEvent.ACTION_DRAG_EXITED:
                return true;

            case DragEvent.ACTION_DROP:

                ClipData.Item item = dragEvent.getClipData().getItemAt(0);
                Log.d("DragEvent", view.getTag().toString());
                char a;
                switch (view.getTag().toString()) {
                    case "FirstAnswer":
                        a = dragEvent.getClipDescription().getLabel().toString().charAt(0);
                        mExerciseRepository.addToFirstAnswer(Integer.valueOf(a));
                        setAdapterView(mFirstAnswerRecyclerView,mExerciseRepository.getmFirstAnswer(),true);
                        break;
                    case "SecondAnswer":
                        a = dragEvent.getClipDescription().getLabel().toString().charAt(0);
                        mExerciseRepository.addToSecondAnswer(Integer.valueOf(a));
                        setAdapterView(mSecondLineRecyclerView, mExerciseRepository.getmSecondAnswer(),true);
                }
                return true;

            case DragEvent.ACTION_DRAG_ENDED:

                return true;

            default:
                break;
        }

        return false;
    }

    @Override
    public void onClick(String tip) {
        Context context = getActivity();
        Toast.makeText(context, tip, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(Integer integer) {
        Context context = getActivity();
        mExerciseRepository.removeFromFirstAnswer(integer);
        setAdapterView(mFirstAnswerRecyclerView, mExerciseRepository.getmFirstAnswer(),true);
    }

    @Override
    public void onLongClick(View view) {
        ClipData.Item item = new ClipData.Item(view.getTag().toString());
        Log.d("view en click", view.getTag().toString());
        ClipData data = new ClipData((CharSequence) view.getTag(), new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN}, item);
        View.DragShadowBuilder dragShadowBuilder = new View.DragShadowBuilder(view);
        view.startDragAndDrop(data, dragShadowBuilder, view, 0);
        //view.setVisibility(view.INVISIBLE);
    }


    View.OnClickListener onDetectPressed = v -> {
        if (mNumbersClassifier == null) {
            Log.e(LOG_TAG, "onDetectClick(): Classifier is not initialized");
            return;
        } else if (mFpvPaint.isEmpty()) {
            Toast.makeText(v.getContext(), R.string.please_write_a_digit, Toast.LENGTH_SHORT).show();
            return;
        }

        Bitmap image = mFpvPaint.exportToBitmap(NumbersClassifier.DIM_IMG_SIZE_WIDTH, NumbersClassifier.DIM_IMG_SIZE_HEIGHT);
        Bitmap inverted = ImageUtil.invert(image);
        Result result = mNumbersClassifier.classify(inverted);
        mExerciseRepository.addToFirstAnswer(result.getNumber());
        setAdapterView(mFirstAnswerRecyclerView, mExerciseRepository.getmFirstAnswer(),true);
        mFpvPaint.clear();
    };

    View.OnClickListener onCleanPressed = v -> {
        mFpvPaint.clear();
    };

    View.OnClickListener onTimer = v -> {
        mExerciseRepository.generateNewExercise(3);
        updateView();
        createCountdown();
    };
    public class ExerciseCountdown extends CountDownTimerWithPause{

        public ExerciseCountdown(long millisOnTimer, long countDownInterval, boolean runAtStart) {
            super(millisOnTimer, countDownInterval, runAtStart);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            mTimerView.setText(String.valueOf(mExerciseRepository.getmTime()));
            mExerciseRepository.timerTick();
        }

        @Override
        public void onFinish() {
            mTimerView.setText("Finish");
            mExerciseRepository.setmCountdownCreated(false);
        }
    }
}
