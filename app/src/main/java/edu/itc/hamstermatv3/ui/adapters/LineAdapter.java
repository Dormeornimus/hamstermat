package edu.itc.hamstermatv3.ui.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import edu.itc.hamstermatv3.utilities.ViewUtils;
import edu.itc.hamstermatv3.R;


//TODO change OnClickHandler
public class LineAdapter extends RecyclerView.Adapter<LineAdapter.LineAdapterViewHolder> {
    private List<Integer> mNumberData;
    private final LineAdapterOnClickHandler mClickHandler;
    private boolean mDragable;

    public interface LineAdapterOnClickHandler{
        void onClick(String string);
        void onClick(Integer integer);
        void onLongClick(View view);
    }

    public LineAdapter(LineAdapterOnClickHandler clickHandler, boolean dragable) {
        mClickHandler = clickHandler;
        mDragable = dragable;
    }

    public class LineAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener ,View.OnLongClickListener{
        public final ImageView mNumberImage;
        public final boolean mDragable;

        public LineAdapterViewHolder(View view,boolean dragable){
            super(view);
            mDragable = dragable;
            mNumberImage = (ImageView) view.findViewById(R.id.numberImageView);
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            String currentNumber =  String.valueOf(mNumberData.get(adapterPosition));
            mClickHandler.onClick(adapterPosition);
        }

        @SuppressLint("NewApi")
        @Override
        public boolean onLongClick(View view) {
            if(mDragable){
                int adapterPosition = getAdapterPosition();
                String currentNumber =  String.valueOf(mNumberData.get(adapterPosition));
                view.setTag(currentNumber);
                Log.d("View holder numero: ",String.valueOf(mNumberData.get(adapterPosition)));
                mClickHandler.onLongClick(view);
            }
            return false;
        }

    }

    @Override
    @NonNull
    public LineAdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.number_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToparentImmediately = false;
        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToparentImmediately);
        return new LineAdapterViewHolder(view,mDragable);
    }

    @Override
    public void onBindViewHolder(LineAdapterViewHolder lineAdapterViewHolder, int position) {
        char numberToImage = mNumberData.get(position).toString().charAt(0);
        lineAdapterViewHolder.mNumberImage.setImageResource(ViewUtils.getResourceIdFromDrawable(numberToImage));
    }

    @Override
    public int getItemCount() {
        if (null == mNumberData) return 0;
        return mNumberData.size();
    }

    public void setNumberData(List<Integer> numberData){
        mNumberData = numberData;
        notifyDataSetChanged();
    }

}


