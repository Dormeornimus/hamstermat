package edu.itc.hamstermatv3.db.entity;


import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import edu.itc.hamstermatv3.model.User;

@Entity(tableName = "users")
public class UserEntity implements User {
    @PrimaryKey
    private int id;
    private int age;
    private int level;
    private String password;
    private String name;
    private String school;

    public UserEntity() {
    }

    @Ignore
    public UserEntity(int id, int age, int level, String password, String name, String school){
        this.id = id;
        this.age = age;
        this.level = level;
        this.password = password;
        this.name = name;
        this.school = school;
    }

    public UserEntity(User user){
        this.id = user.getId();
        this.age = user.getAge();
        this.level = user.getLevel();
        this.password = user.getPassword();
        this.name = user.getName();
        this.school = user.getSchool();
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public int getLevel() {
        return level;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSchool() {
        return school;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSchool(String school) {
        this.school = school;
    }
}
