package edu.itc.hamstermatv3.db.dao;


import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import edu.itc.hamstermatv3.db.entity.UserEntity;

@Dao
public interface UserDao {
    @Query("SELECT * FROM users")
    LiveData<List<UserEntity>> loadAllUsers();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<UserEntity> users);

    @Query("SELECT * FROM users where id = :userId")
    LiveData<UserEntity> loadUserById(int userId);

    @Query("select * from users where id = :userId")
    UserEntity loadUserByIdSync(int userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUser(UserEntity user);

    @Update
    void updateUser (UserEntity user);

    @Delete
    void deleteUser (UserEntity user);
}
