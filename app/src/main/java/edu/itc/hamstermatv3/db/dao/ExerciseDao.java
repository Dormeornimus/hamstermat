/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.itc.hamstermatv3.db.dao;


import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import edu.itc.hamstermatv3.db.entity.ExerciseEntity;

@Dao
public interface ExerciseDao {
    @Query("SELECT * FROM exercises")
    LiveData<List<ExerciseEntity>> loadAllExercises();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<ExerciseEntity> exercises);

    @Query("select * from exercises where id = :exerciseId")
    LiveData<ExerciseEntity> loadExerciseById(int exerciseId);

    @Query("select * from exercises where id = :exerciseId")
    ExerciseEntity loadExerciseByIdSync(int exerciseId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertExercise(ExerciseEntity exercise);

    @Update
    void updateExercise (ExerciseEntity exercise);

    @Delete
    void deleteExercise (ExerciseEntity exercise);
}
