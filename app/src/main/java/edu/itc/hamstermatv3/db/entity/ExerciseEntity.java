/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.itc.hamstermatv3.db.entity;

import java.util.Date;
import java.util.List;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import edu.itc.hamstermatv3.model.Exercise;

@Entity(tableName = "exercises",foreignKeys = {
        @ForeignKey(entity = UserEntity.class,
        parentColumns = "id",
        childColumns =  "userId",
        onDelete = ForeignKey.CASCADE)},
        indices = {@Index(value="userId")
        })

public class ExerciseEntity implements Exercise {
    @PrimaryKey
    private int id;
    private int userId;
    private String firstOperand;
    private String secondOperand;
    private String operator; //Check for enum
    @Ignore
    private List<String> correctAnswers;
    @Ignore
    private List<String> answers;
    private String emotion;
    private int mistakes;
    private Date solvedAt;

    @Override
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ExerciseEntity() {
    }

    @Ignore
    public ExerciseEntity(int id, int usedId, String firstOperand, String secondOperand, String operator, List<String> correctAnswers, List<String> answers, String emotion, int mistakes, Date solvedAt ) {
        this.id = id;
        this.userId = usedId;
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
        this.operator = operator;
        this.correctAnswers = correctAnswers;
        this.answers = answers;
        this.emotion = emotion;
        this.mistakes = mistakes;
        this.solvedAt = solvedAt;
    }

    public ExerciseEntity(Exercise exercise) {
        this.id = exercise.getId();
        this.userId = exercise.getUserId();
        this.firstOperand = exercise.getFirstOperand();
        this.secondOperand = exercise.getSecondOperand();
        this.operator = exercise.getOperator();
        this.correctAnswers = getCorrectAnswers();
        this.answers = exercise.getAnswers();
        this.emotion = exercise.getEmotion();
        this.mistakes = exercise.getMistakes();
        this.solvedAt = exercise.getSolvedAt();
    }

    @Override
    public String getFirstOperand() {
        return firstOperand;
    }

    public void setFirstOperand(String firstOperand) {
        this.firstOperand = firstOperand;
    }

    @Override
    public String getSecondOperand() {
        return secondOperand;
    }

    public void setSecondOperand(String secondOperand) {
        this.secondOperand = secondOperand;
    }

    @Override
    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Override
    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

    @Override
    public List<String> getCorrectAnswers() {
        return correctAnswers;
    }

    public void setCorrectAnswers(List<String> correctAnswers) {
        this.correctAnswers = correctAnswers;
    }

    @Override
    public String getEmotion() {
        return emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }

    @Override
    public Integer getMistakes() {
        return mistakes;
    }

    public void setMistakes(Integer mistakes) {
        this.mistakes = mistakes;
    }

    @Override
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public Date getSolvedAt() {
        return solvedAt;
    }

    public void setSolvedAt(Date solvedAt) {
        this.solvedAt = solvedAt;
    }
}
