package edu.itc.hamstermatv3.db;

import android.util.Log;

import java.util.LinkedList;
import java.util.List;

import edu.itc.hamstermatv3.utilities.ProblemsGenerator;

public class ExerciseRepository {

    private static ExerciseRepository sInstance;
    private List<Integer> mFirstOperand;
    private List<Integer> mSecondOperand;
    private ProblemsGenerator mProblemsGenerator;
    private LinkedList<Integer> mFirstAnswer;
    private LinkedList<Integer> mSecondAnswer;
    private LinkedList<Integer> mThirdAnswer;
    private LinkedList<Integer> mCorrectAnswers;
    private LinkedList<Integer> mAnswers;
    private String mEmotion;
    private Integer mErrors;
    private Integer mHelps;
    private Integer mTime;
    private boolean mCountdownCreated;

    //TODO work with implementation of Revision system
    //TODO implement the inference system
    //TODO finish first publishing.

    private ExerciseRepository(){
        mProblemsGenerator = new ProblemsGenerator();
        mFirstAnswer = new LinkedList<>();
        mSecondAnswer = new LinkedList<>();
        mThirdAnswer = new LinkedList<>();
        mCorrectAnswers = new LinkedList<>();
        mAnswers = new LinkedList<>();
        generateNewExercise(3);
    }

    public void generateNewExercise(Integer level){
        mProblemsGenerator.startMakingNewProblem(level);
        mFirstOperand = mProblemsGenerator.getmFirstOperandList();
        mSecondOperand = mProblemsGenerator.getmSecondOperandList();
        mErrors = 0;
        mHelps = 0;
        mTime = 60;
        calculateAnswers();
        Log.d("First operand",mProblemsGenerator.getmFirstOperand().toString());
        Log.d("Second operand",mProblemsGenerator.getmSecondOperand().toString());
        Log.d("Answers",mCorrectAnswers.toString());
    }
    public static ExerciseRepository getInstance() {
        if (sInstance == null) {
            synchronized (ExerciseRepository.class) {
                if (sInstance == null) {
                    sInstance = new ExerciseRepository();
                }
            }
        }
        return sInstance;
    }


    public List<Integer> getmFirstOperand() {
        return mFirstOperand;
    }

    public void setmFirstOperand(List<Integer> mFirstOperand) {
        this.mFirstOperand = mFirstOperand;
    }

    public List<Integer> getmSecondOperand() {
        return mSecondOperand;
    }

    public void setmSecondOperand(List<Integer> mSecondOperand) {
        this.mSecondOperand = mSecondOperand;
    }

    public LinkedList<Integer> getmFirstAnswer() {
        return mFirstAnswer;
    }

    public void addToFirstAnswer(Integer number) {
        this.mFirstAnswer.addFirst(number);
    }

    public void removeFromFirstAnswer(int number){
        if(mFirstAnswer.size()>number){
            Log.d("Before",mFirstAnswer.toString());
            this.mFirstAnswer.remove(number);
            Log.d("After",mFirstAnswer.toString());
        }
    }

    public LinkedList<Integer> getmSecondAnswer() {
        return mSecondAnswer;
    }

    public void addToSecondAnswer(Integer number) {
        this.mSecondAnswer.addFirst(number);
    }

    public LinkedList<Integer> getmThirdAnswer() {
        return mThirdAnswer;
    }

    public void AddToThirdAnswer(Integer number) {
        this.mThirdAnswer.add(number);
    }

    public LinkedList<Integer> getmCorrectAnswers() {
        return mCorrectAnswers;
    }

    public void setmCorrectAnswers(LinkedList<Integer> mCorrectAnswers) {
        this.mCorrectAnswers = mCorrectAnswers;
    }

    public LinkedList<Integer> getmAnswers() {
        return mAnswers;
    }

    public void setmAnswers(LinkedList<Integer> mAnswers) {
        this.mAnswers = mAnswers;
    }

    public String getmEmotion() {
        return mEmotion;
    }

    public void setmEmotion(String mEmotion) {
        this.mEmotion = mEmotion;
    }

    public Integer getmErrors() {
        return mErrors;
    }

    public void setmErrors(Integer mErrors) {
        this.mErrors = mErrors;
    }

    public Integer getmHelps() {
        return mHelps;
    }

    public void addmTips(){
        mHelps++;
    }

    public void setmHelps(Integer mHelps) {
        this.mHelps = mHelps;
    }

    public void calculateAnswers(){
        Log.d("First operand",mProblemsGenerator.getmFirstOperand().toString());
        Integer tempAnswer =mProblemsGenerator.getmFirstOperand() + mProblemsGenerator.getmSecondOperand();
        Log.d("tempAnswer",tempAnswer.toString());
        mCorrectAnswers.addAll(mProblemsGenerator.numberList(tempAnswer));
    }

    public Integer getmTime() {
        return mTime;
    }

    public void setmTime(Integer mTime) {
        this.mTime = mTime;
    }

    public void timerTick(){
        this.mTime--;
    }

    public boolean ismCountdownCreated() {
        return mCountdownCreated;
    }

    public void setmCountdownCreated(boolean mCountdownCreated) {
        this.mCountdownCreated = mCountdownCreated;
    }
}
