package edu.itc.hamstermatv3.db;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import edu.itc.hamstermatv3.db.entity.CommentEntity;
import edu.itc.hamstermatv3.db.entity.ExerciseEntity;
import edu.itc.hamstermatv3.db.entity.UserEntity;

/**
 * Repository handling the work with products and comments.
 */
public class DataRepository {

    private static DataRepository sInstance;

    private final AppDatabase mDatabase;
    private MediatorLiveData<List<ExerciseEntity>> mObservableExercises;
    private MediatorLiveData<List<UserEntity>> mObservableUsers;

    private DataRepository(final AppDatabase database) {
        mDatabase = database;
        mObservableExercises = new MediatorLiveData<>();

        mObservableExercises.addSource(mDatabase.productDao().loadAllExercises(),
                exerciseEntities -> {
                    if (mDatabase.getDatabaseCreated().getValue() != null) {
                        mObservableExercises.postValue(exerciseEntities);
                    }
                });

        mObservableUsers.addSource(mDatabase.userDao().loadAllUsers(),
                userEntities -> {
                    if(mDatabase.getDatabaseCreated().getValue() !=null){
                        mObservableUsers.postValue(userEntities);
                    }
                });
    }

    public static DataRepository getInstance(final AppDatabase database) {
        if (sInstance == null) {
            synchronized (DataRepository.class) {
                if (sInstance == null) {
                    sInstance = new DataRepository(database);
                }
            }
        }
        return sInstance;
    }

    /**
     * Get the list of products from the database and get notified when the data changes.
     */
    public LiveData<List<ExerciseEntity>> getProducts() {
        return mObservableExercises;
    }

    public LiveData<ExerciseEntity> loadProduct(final int exerciseId) {
        return mDatabase.productDao().loadExerciseById(exerciseId);
    }

    public LiveData<List<CommentEntity>> loadComments(final int exerciseId) {
        return mDatabase.commentDao().loadCommentsByExerciseId(exerciseId);
    }

    public LiveData<List<UserEntity>> loadUsers(){
        return mDatabase.userDao().loadAllUsers();
    }
}
