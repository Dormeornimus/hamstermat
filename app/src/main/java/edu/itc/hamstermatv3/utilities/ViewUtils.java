package edu.itc.hamstermatv3.utilities;

import edu.itc.hamstermatv3.R;

public class ViewUtils
{
    static public int getResourceIdFromDrawable (char instanceNumber){
        switch(instanceNumber){
            case '0':
               return R.drawable.number0;
            case '1':
                return R.drawable.number1;
            case '2':
                return R.drawable.number2;
            case '3':
                return R.drawable.number3;
            case '4':
                return R.drawable.number4;
            case '5':
                return R.drawable.number5;
            case '6':
                return R.drawable.number6;
            case '7':
                return R.drawable.number7;
            case '8':
                return R.drawable.number8;
            case '9':
                return R.drawable.number9;
        }
        return 0;
    }
}
