package edu.itc.hamstermatv3.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ProblemsGenerator {

    private Integer firstDigitUp;
    private Integer firstDigitDown;
    private Integer secondDigitUp;
    private Integer secondDigitDown;
    private Integer thirdDigitUp;
    private Integer mFirstOperand;
    private List<Integer> mFirstOperandList;
    private List<Integer> mSecondOperandList;
    private Integer mSecondOperand;

    public Integer getmFirstOperand(){
        return this.mFirstOperand;
    }
    public Integer getmSecondOperand(){
        return this.mSecondOperand;
    }
    public Integer getFirstDigitUp(){
        return this.firstDigitUp;
    }
    public Integer getSecondDigitUp(){
        return this.secondDigitUp;
    }
    public Integer getThirdDigitUp(){
        return this.thirdDigitUp;
    }
    public Integer getFirstDigitDown(){
        return this.firstDigitDown;
    }
    public Integer getSecondDigitDown(){
        return this.secondDigitDown;
    }

    public void startMakingNewProblem(Integer level){
        firstDigitUp=0;
        firstDigitDown=0;
        secondDigitUp=0;
        secondDigitDown=0;
        thirdDigitUp=0;
        Integer upperNum= firstOperand(level);
        Integer lowerNum= secondOperand(level);
        mFirstOperand =upperNum;
        mSecondOperand =lowerNum;
        switch(level){
            case 1:
                firstDigitUp=upperNum;
                firstDigitDown=lowerNum;
                break;
            case 2:
                firstDigitUp=breakDownNumber(upperNum,1,0);
                secondDigitUp=breakDownNumber(upperNum,0,1);
                firstDigitDown=lowerNum;
                break;
            case 3:
                firstDigitUp=breakDownNumber(upperNum,2,0);
                secondDigitUp=breakDownNumber(upperNum,1,1);
                thirdDigitUp=breakDownNumber(upperNum,0,2);
                firstDigitDown=breakDownNumber(lowerNum,1,0);
                secondDigitDown=breakDownNumber(lowerNum,0,1);
        }
        mFirstOperandList = numberList(mFirstOperand);
        mSecondOperandList = numberList(mSecondOperand);
    }

    private Integer breakDownNumber(Integer toBreak,Integer digitWanted,Integer zeroToAdd){
        String temporal;
        Integer returnValue;
        String charNum=toBreak.toString();
        switch(zeroToAdd){
            case 0:
                temporal=new StringBuilder().append(charNum.charAt(digitWanted)).toString();
                returnValue=Integer.valueOf(temporal);
                return returnValue;
            case 1:
                temporal=new StringBuilder().append(charNum.charAt(digitWanted)+"0").toString();
                returnValue=Integer.valueOf(temporal);
                return returnValue;
            case 2:
                temporal=new StringBuilder().append(charNum.charAt(digitWanted)+"00").toString();
                returnValue=Integer.valueOf(temporal);
                return returnValue;
        }
        return returnValue=0;
    }

    public List<Integer> numberList(Integer toBreak){
         char[] temp = toBreak.toString().toCharArray();
         List<Integer> numberback = new ArrayList<>();
        for(char o : temp){
            numberback.add(Character.getNumericValue(o));
        }
        return numberback;
    }

    private Integer firstOperand(Integer level){
        Integer returningValue;
        Random randomize=new Random();
        switch(level){
            case 1:
                returningValue=randomize.nextInt(9)+1;
                return returningValue;
            case 2:
                returningValue=randomize.nextInt(89)+10;
                return returningValue;
            case 3:
                returningValue=randomize.nextInt(899)+100;
                return returningValue;
        }
        return returningValue=0;
    }

    private Integer secondOperand(Integer level){
        Integer returningValue;
        Random randomize=new Random();
        switch(level){
            case 1:
                returningValue=randomize.nextInt(9)+1;
                return returningValue;
            case 2:
                returningValue=randomize.nextInt(9)+1;
                return returningValue;
            case 3:
                returningValue=randomize.nextInt(89)+10;
                return returningValue;
        }
        return returningValue=0;
    }

    public List<Integer> getmFirstOperandList() {
        return mFirstOperandList;
    }

    public void setmFirstOperandList(List<Integer> mFirstOperandList) {
        this.mFirstOperandList = mFirstOperandList;
    }

    public List<Integer> getmSecondOperandList() {
        return mSecondOperandList;
    }

    public void setmSecondOperandList(List<Integer> mSecondOperandList) {
        this.mSecondOperandList = mSecondOperandList;
    }
}

