/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.itc.hamstermatv3.utilities;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import edu.itc.hamstermatv3.db.entity.CommentEntity;
import edu.itc.hamstermatv3.db.entity.ExerciseEntity;
import edu.itc.hamstermatv3.db.entity.UserEntity;
import edu.itc.hamstermatv3.model.Exercise;

/**
 * Generates data to pre-populate the database
 */
public class DataGenerator {

    /*private static final String[] FIRST = new String[]{
            "Special edition", "New", "Cheap", "Quality", "Used"};
    private static final String[] SECOND = new String[]{
            "Three-headed Monkey", "Rubber Chicken", "Pint of Grog", "Monocle"};
    private static final String[] DESCRIPTION = new String[]{
            "is finally here", "is recommended by Stan S. Stanman",
            "is the best sold exercise on Mêlée Island", "is \uD83D\uDCAF", "is ❤️", "is fine"};*/
    private static final String[] COMMENTS = new String[]{
            "Comment 1", "Comment 2", "Comment 3", "Comment 4", "Comment 5", "Comment 6"};


    public static List<ExerciseEntity> generateExercises(){
        return null;
    }

    public static List<UserEntity> generateUsers(){
        List<UserEntity> users = new LinkedList<>();
        UserEntity hector = new UserEntity();
        hector.setAge(25);
        hector.setName("Hector Cardenas");
        hector.setLevel(0);
        hector.setPassword("0");
        hector.setSchool("Tecnologico de Culiacan");
        users.add(hector);
        return users;
    }

    /*public static List<ExerciseEntity> generateExercises() {
        List<ExerciseEntity> products = new ArrayList<>(FIRST.length * SECOND.length);
        Random rnd = new Random();
        for (int i = 0; i < FIRST.length; i++) {
            for (int j = 0; j < SECOND.length; j++) {
                ExerciseEntity exercise = new ExerciseEntity();
                exercise.setName(FIRST[i] + " " + SECOND[j]);
                exercise.setDescription(exercise.getName() + " " + DESCRIPTION[j]);
                exercise.setPrice(rnd.nextInt(240));
                exercise.setId(FIRST.length * i + j + 1);
                products.add(exercise);
            }
        }
        return products;
    }
    */

    public static List<CommentEntity> generateCommentsForProducts(
            final List<ExerciseEntity> products) {
        List<CommentEntity> comments = new ArrayList<>();
        Random rnd = new Random();

        for (Exercise exercise : products) {
            int commentsNumber = rnd.nextInt(5) + 1;
            for (int i = 0; i < commentsNumber; i++) {
                CommentEntity comment = new CommentEntity();
                comment.setExerciseId(exercise.getId());
                comment.setText(COMMENTS[i] + " for " + exercise.getId());
                comment.setPostedAt(new Date(System.currentTimeMillis()
                        - TimeUnit.DAYS.toMillis(commentsNumber - i) + TimeUnit.HOURS.toMillis(i)));
                comments.add(comment);
            }
        }

        return comments;
    }
}
