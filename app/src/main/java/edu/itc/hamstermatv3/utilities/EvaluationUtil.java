package edu.itc.hamstermatv3.utilities;

import java.util.List;

public interface EvaluationUtil {
    public List<String> solve();
}
