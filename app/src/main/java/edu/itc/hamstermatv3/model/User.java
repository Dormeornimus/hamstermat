package edu.itc.hamstermatv3.model;

public interface User {
    int getId();
    int getAge();
    int getLevel();
    String getPassword();
    String getName();
    String getSchool();
}
